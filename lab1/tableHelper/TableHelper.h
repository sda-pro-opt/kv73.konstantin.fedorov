#ifndef OPTCOURCE_TABLEHELPER_H
#define OPTCOURCE_TABLEHELPER_H

#include <map>

using namespace std;

class TableHelper {
    map<int, string> codeToLexemMap;
    map<string, int> lexemToCodeMap;

    public:
        TableHelper(): codeToLexemMap(), lexemToCodeMap() {};
        bool isLexemAdded(string *);
        void addLexemToMap(int, string);
        map<string, int> getLexemToCodeMap();
        map<int, string> getCodeToLexemMap();
        void print();
        std::string printToFile();
    };


#endif //OPTCOURCE_TABLEHELPER_H
