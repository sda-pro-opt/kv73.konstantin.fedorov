#include "TableHelper.h"

#include "iostream"
#include <string>

bool TableHelper::isLexemAdded(std::string *lexem) {
    return this->lexemToCodeMap.find(*lexem) != this->lexemToCodeMap.end();
}

void TableHelper::addLexemToMap(int code, std::string lexem) {
    if (!this->isLexemAdded(&lexem)) {
        this->codeToLexemMap.insert(std::pair<int,std::string>(code, lexem));
        this->lexemToCodeMap.insert(std::pair<std::string, int>(lexem, code));
    }
}

map<string, int> TableHelper::getLexemToCodeMap() {
    return this->lexemToCodeMap;
}

map<int, string> TableHelper::getCodeToLexemMap() {
    return  this->codeToLexemMap;
}

void TableHelper::print() {
    for(const auto& it: codeToLexemMap) {
        std::cout << "Code: " << it.first << " | Lexem: " <<it.second << "\n";
    }
}

std::string TableHelper::printToFile() {
    std::string table = "";
    for(const auto& it: codeToLexemMap) {
        table = table + "Code: " + std::to_string(it.first) + " | Lexem: " + it.second + "\n";
    }

    return table + "-------------------------------------------------------------\n";
}
