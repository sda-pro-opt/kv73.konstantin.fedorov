cmake_minimum_required(VERSION 3.15)
project(OPTcource)

set(CMAKE_CXX_STANDARD 17)

add_executable(lab1 main.cpp analyzer/Analyzer.cpp analyzer/Analyzer.h fileIO/FileIOHelper.cpp fileIO/FileIOHelper.h tableHelper/TableHelper.cpp tableHelper/TableHelper.h charPositionHelper/CharPositionHelper.cpp charPositionHelper/CharPositionHelper.h models/Models.h)