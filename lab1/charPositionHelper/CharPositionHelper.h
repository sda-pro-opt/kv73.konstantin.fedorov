#ifndef OPTCOURCE_CHARPOSITIONHELPER_H
#define OPTCOURCE_CHARPOSITIONHELPER_H

#include "../models/Models.h"

class CharPositionHelper {
    Position position;

    public:
        void moveToNextRow();
        void move();
        Position getCurrentPosition();
};


#endif //OPTCOURCE_CHARPOSITIONHELPER_H
