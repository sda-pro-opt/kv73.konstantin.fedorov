#include "CharPositionHelper.h"

void CharPositionHelper::move() {
    ++this->position.currentColumn;
}

void CharPositionHelper::moveToNextRow() {
    this->position.currentColumn = 0;
    ++this->position.currentRow;
}

Position CharPositionHelper::getCurrentPosition() {
    return this->position;
}