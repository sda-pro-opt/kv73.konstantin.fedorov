#ifndef OPTCOURCE_MODELS_H
#define OPTCOURCE_MODELS_H

#include "string"

using namespace std;

struct Position {
    int currentRow = 0;
    int currentColumn = 0;
};

struct Symbol {
    char value;
    int code;
    int type;
    Position position;
};

struct Lexem {
    string lexem;
    Position position;
};

// I want kotlin Data classes :(
//
//data class Token(
//    val code,
//    val row,
//    val col
//)
struct Token {
    int mCode;
    int mRow;
    int mColumn;

    void set(int code, int row, int column) {
        mCode = code;
        mRow = row ;
        mColumn = column;
    }
};

enum Categories {
    WHITESPACE,
    DIGIT,
    LETTER,
    DELIMITER,
    MULTI_DELIMITER,
    COMMENT,
    ERR,
};

//need to use namespace to avoid colisions with Categories
namespace ent {
    enum Entities {
        DELIMITER,
        MULTI_DELIMITER,
        KEY_LEXEM,
        CONSTANT,
        IDENTIFIER
    };
}

#endif //OPTCOURCE_MODELS_H