#include <vector>
#include <optional>

#include "analyzer/Analyzer.h"
#include "fileIO/FileIOHelper.h"
#include "charPositionHelper/CharPositionHelper.h"

int scan(std::string filePath, std::string outPath);

int main() {
    std::string filePath1 = "../tests/test1.sig";
    std::string outPath1 = "../tests/expected1.txt";

    std::string filePath2 = "../tests/test2.sig";
    std::string outPath2 = "../tests/expected2.txt";

    std::string filePath3 = "../tests/test3.sig";
    std::string outPath3 = "../tests/expected3.txt";

    scan(filePath1, outPath1);
    scan(filePath2, outPath2);
    scan(filePath3, outPath3);

    return 0;
}

int scan(std::string filePath, std::string outPath) {
    auto * tableHelper = new TableHelper();
    auto * fileIOHelper = new FileIOHelper(filePath, outPath);

    auto *scanner = new Analyzer(
            tableHelper,
            fileIOHelper,
            new CharPositionHelper()
    );

    std::optional<std::vector<Token>> oTokens = scanner->doScanning();

    if (!oTokens) return 1;

    std::string results = tableHelper->printToFile();
    for(auto code: oTokens.value()) {
        results +=
                "-----------------------------\n"
                "Code: " + to_string(code.mCode) + "\n"+
                +  "Lexem: "+ tableHelper->getCodeToLexemMap().find(code.mCode)->second + "\n"+
                +  "Row: " + to_string(code.mRow) + "\n"+
                +  "Column: " + to_string(code.mColumn) + "\n";
    }
    fileIOHelper->writeToFile(results);

    return 0;
}
