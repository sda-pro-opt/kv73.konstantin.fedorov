#include <iostream>
#include "FileIOHelper.h"

FileIOHelper::FileIOHelper(std::string fileReadPath, std::string fileWritePath) : readStream(fileReadPath), writeStream(fileWritePath, std::ios::trunc){};

std::optional<char> FileIOHelper::readChar() {
    if (this->readStream.eof()) {
        this->readStream.close();
        return std::nullopt;
    }

    char value = this->readStream.get();
    if (value == -1) {
        this->readStream.close();
        return std::nullopt;
    }
    return value;
}

void FileIOHelper::writeToFile(std::string data) {
    writeStream << data;
    writeStream.close();
}
