#ifndef OPTCOURCE_FILEIOHELPER_H
#define OPTCOURCE_FILEIOHELPER_H

#include <fstream>
#include <optional>

class FileIOHelper {
    std::ifstream readStream;
    std::ofstream writeStream;

public:
    FileIOHelper(std::string, std::string);
    std::optional<char> readChar();
    void writeToFile(std::string);
};


#endif //OPTCOURCE_FILEIOHELPER_H
