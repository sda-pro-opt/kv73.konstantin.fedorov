#include "Analyzer.h"
#include <iostream>
#include "map"

using namespace std;

void Analyzer::createCategoryArray() {
    for(int & i : categoriesArr) {
        i = ERR;
    }
    for(int i = 48; i < 57; i++) {
        categoriesArr[i] = DIGIT;
    }
    for(int i = 65; i < 90; i++) {
        categoriesArr[i] = LETTER;
    }
    categoriesArr[40] = DELIMITER;//(
    categoriesArr[41] = DELIMITER;//)
    categoriesArr[46] = DELIMITER;
    categoriesArr[59] = DELIMITER;
    categoriesArr[45] = DELIMITER;
    categoriesArr[44] = DELIMITER;//,

    categoriesArr[9] = WHITESPACE;
    categoriesArr[10] = WHITESPACE;
    categoriesArr[11] = WHITESPACE;
    categoriesArr[12] = WHITESPACE;
    categoriesArr[13] = WHITESPACE;
    categoriesArr[32] = WHITESPACE;
}

int Analyzer::getCategoryBySymbol(int symbol) {
    return categoriesArr[symbol];
}

map<int, Analyzer::codeRange> Analyzer::initGenerators() {

    const codeRange delimiterRange = {0, 255};
    const codeRange multiDelimiterRange = {301, 400 };
    const codeRange constantRange = {401, 500 };
    const codeRange keyLexemsRange = { 501, 600 };
    const codeRange identifierRange = { 601, 700 };

    return map<int, codeRange> {
            { ent::DELIMITER, delimiterRange },
            { ent::MULTI_DELIMITER, multiDelimiterRange },
            { ent::KEY_LEXEM, keyLexemsRange },
            { ent::CONSTANT, constantRange },
            { ent::IDENTIFIER, identifierRange }
    };
}

Analyzer::Analyzer(
        TableHelper *tableHelper,
        FileIOHelper *fileIOHelper,
        CharPositionHelper *charPositionHelper
) {
    this->tableHelper = tableHelper;
    this->fileIOHelper = fileIOHelper;
    this->charPositionHelper = charPositionHelper;

    createCategoryArray();
};

std::optional<vector<Token>> Analyzer::doScanning() {
    map<int, int> entityByCategory ={
                                        { LETTER, ent::IDENTIFIER },
                                        { DIGIT, ent::CONSTANT },
                                        { DELIMITER, ent::DELIMITER },
                                        { MULTI_DELIMITER, ent::MULTI_DELIMITER }
                                    };
    map<int, codeRange> generatorByEntity = this->initGenerators(); //CLEAN
    const map<int, vector<string>> reservedLexems = {
            { ent::DELIMITER, { ";", ","} },
            { ent::KEY_LEXEM, { "PROCEDURE", "BEGIN", "END", "RETURN" } }
    };
    for (const auto& entry : reservedLexems) {
        for (const auto& lexem : entry.second) {
            int code = getCode((generatorByEntity) [entry.first].current);
            (generatorByEntity)[entry.first].current++;
            this->tableHelper->addLexemToMap(code, lexem);
        }

    }

    vector<Symbol> buffer;
    vector<Symbol> errBuffer;

    Symbol currentSymbol;

    std::optional<Symbol> oSymbol = getNext();
    while (oSymbol) {
        currentSymbol = oSymbol.value();

        switch (currentSymbol.type) {
            case DIGIT: {
                int size = buffer.size();
                if (size) {
                    int prevSymbolType = buffer[size - 1].type;
                    if (prevSymbolType == DELIMITER) {
                        this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                    }
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->getNext();
                break;
            }
            case LETTER: {
                int size = buffer.size();
                if (size) {
                    int prevSymbolType = buffer[size - 1].type;
                    if (prevSymbolType == DELIMITER ) {
                        this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                    }
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->getNext();
                break;
            }
            case MULTI_DELIMITER: {
                //Нема
            }
            case DELIMITER: {
                oSymbol = this->getNext();
                Symbol nextSymbol = oSymbol.value();

                if (currentSymbol.value == '(') {
                    //process comment
                    if(nextSymbol.value == '*') {
                        currentSymbol.type = COMMENT;
                        nextSymbol.type = COMMENT;
                        buffer.push_back(currentSymbol);
                        buffer.push_back(nextSymbol);
                        oSymbol.emplace(nextSymbol);
                        break;
                    }
                }
                if (!buffer.empty()) {
                    this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                }
                buffer.push_back(currentSymbol);
                break;
            }
            case WHITESPACE: {
                if (!buffer.empty()) {
                    this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                }
                if (currentSymbol.value == '\n') {
                    this->charPositionHelper->moveToNextRow();
                } else {
                    for(int i = 0; i < buffer.size(); i++) {
                        this->charPositionHelper->move();
                    }
                }
                oSymbol= this->getNext();
                break;
            }
            //del?
            case COMMENT: {
                bool exit = false;
                oSymbol = getNext();
                buffer.clear();
                if (!oSymbol) { // if eof
                    break;
                }
                while (oSymbol) {
                    currentSymbol = oSymbol.value();
                    if (currentSymbol.value == '*') {
                        oSymbol = getNext();
                        if (oSymbol) {
                            currentSymbol = oSymbol.value();
                            if (currentSymbol.value == ')') {
                                oSymbol = getNext();
                                exit = true;
                                break;
                            }
                        }
                    } else {
                        if (currentSymbol.value == '\n') {
                            this->charPositionHelper->moveToNextRow();
                        }
                        oSymbol = getNext();
                    }
                }
                if (!exit) {
                    printError("ERR:UNCLOSED COMMENT", &currentSymbol);
                    errBuffer.push_back(currentSymbol);
                }
                break;
            }
            case ERR: {
                printError("ERR:ILLEGAL SYMBOL", &currentSymbol);
                errBuffer.push_back(currentSymbol);
                oSymbol = this->getNext();
                break;
            }
        }
    }

    if (!errBuffer.empty()) {
        return nullopt;
    }

    this->tableHelper->print();

    return this->codes;
}


int Analyzer::getCode(int prevCode) {
    return prevCode++;
}

std::optional<Symbol>Analyzer::getNext() {
    this->charPositionHelper->move();

    std::optional<char> symbol = this->fileIOHelper->readChar();
    if (!symbol) return std::nullopt;

    Position symbolPosition = this->charPositionHelper->getCurrentPosition();

    char value = symbol.value();
    return Symbol{
            .value = value,
            .code = value,
            .type = getCategoryBySymbol(value),
            .position = symbolPosition
    };

}

void Analyzer::insertLexemToTable(
        map<int, int> *entityByCategory,
        map<int, codeRange> *generatorByEntity,
        vector<Symbol> *symbols
) {
    Lexem tempLexem = this->createStringFromVector(symbols);
    int firstSymbolCategory = (*symbols)[0].type;
    int code;
    if (this->tableHelper->isLexemAdded(&tempLexem.lexem)) {
        map<string, int> table = this->tableHelper->getLexemToCodeMap();
        code = table[tempLexem.lexem];

    } else {
        int entity = (*entityByCategory)[firstSymbolCategory];
        code = getCode((*generatorByEntity)[entity].current);
        (*generatorByEntity)[entity].current++;
    }

    this->tableHelper->addLexemToMap(code, tempLexem.lexem);
    this->codes.push_back(Token{
            .mCode = code,
            .mRow = tempLexem.position.currentRow,
            .mColumn = tempLexem.position.currentColumn
    });
    symbols->clear();
}

Lexem Analyzer::createStringFromVector(vector<Symbol> *symbols) {
    string lexem;
    for(auto & i : (*symbols)){
        lexem += i.value;
    }
    return Lexem{
            .lexem = lexem,
            .position = (*symbols)[0].position
    };
}

void Analyzer::printSymbol(Symbol *symbol) {
    std::cout << "Symbol: " <<symbol->value << "\n";
    std::cout << "Row: " <<symbol->position.currentRow << "\n";
    std::cout << "Column: " <<symbol->position.currentColumn << "\n\n";

//    this->fileIOHelper->writeSymbolToFile(*symbol)
}

void Analyzer::printSymbols(vector<Symbol> *symbols) {
    for(auto symbol: *symbols) {
        this->printSymbol(&symbol);
    }
}

void Analyzer::printError(const string& errMessage, Symbol *errPlace) {

    std::string results = tableHelper->printToFile();
    
    results += errMessage + "\n"
    +  "Row: " + to_string(errPlace->position.currentRow) +  "\n"
    +  "Column: " + to_string(errPlace->position.currentColumn) +  "\n\n";

    fileIOHelper->writeToFile(results);
}
