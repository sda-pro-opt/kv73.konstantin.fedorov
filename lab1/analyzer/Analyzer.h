#ifndef OPTCOURCE_ANALYZER_H
#define OPTCOURCE_ANALYZER_H

#define CATEGORIES_LENGTH 127

#include "vector"
#include "../fileIO/FileIOHelper.h"
#include "../charPositionHelper/CharPositionHelper.h"
#include "../tableHelper/TableHelper.h"
#include "../models/Models.h"

using namespace std;

class Analyzer {
    vector<Token> codes;

    TableHelper *tableHelper;
    FileIOHelper *fileIOHelper;
    CharPositionHelper *charPositionHelper;

    struct codeRange {
        int from;
        int to;
        int current = from;
    };

    int categoriesArr[CATEGORIES_LENGTH]{};

    Lexem createStringFromVector(vector<Symbol>*);
    std::optional<Symbol> getNext();
    map<int, codeRange> initGenerators();
    void insertLexemToTable(map<int, int>*, map<int, codeRange>*, vector<Symbol>*);
    void printSymbols(vector<Symbol>*);
    void printSymbol(Symbol*);
    void printError(const string& errMessage, Symbol *errPlace);

    void createCategoryArray();
    int getCategoryBySymbol(int symbol);
    int getCode(int prevCode);


    public:
    Analyzer(
            TableHelper*,
            FileIOHelper*,
            CharPositionHelper*
    );
    optional<vector<Token>> doScanning();
};

#endif //OPTCOURCE_ANALYZER_H

